<?php

namespace App\Repository;

use App\Entity\Convert;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Convert|null find($id, $lockMode = null, $lockVersion = null)
 * @method Convert|null findOneBy(array $criteria, array $orderBy = null)
 * @method Convert[]    findAll()
 * @method Convert[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConvertRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Convert::class);
    }

    // /**
    //  * @return Convert[] Returns an array of Convert objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Convert
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
