<?php

namespace App\Entity;

use App\Repository\CurrencyRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CurrencyRepository::class)
 */
class Currency
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $currency;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @ORM\OneToMany(targetEntity=Convert::class, mappedBy="to_currency")
     */
    private $converts;

    public function __construct()
    {
        $this->converts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    public function setCurrency(string $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return Collection|Convert[]
     */
    public function getConverts(): Collection
    {
        return $this->converts;
    }

    public function addConvert(Convert $convert): self
    {
        if (!$this->converts->contains($convert)) {
            $this->converts[] = $convert;
            $convert->setToCurrency($this);
        }

        return $this;
    }

    public function removeConvert(Convert $convert): self
    {
        if ($this->converts->removeElement($convert)) {
            // set the owning side to null (unless already changed)
            if ($convert->getToCurrency() === $this) {
                $convert->setToCurrency(null);
            }
        }

        return $this;
    }
}
