<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ConvertController extends AbstractController
{

    public function index(): Response
    {
        return $this->render('public/convert/number.html.twig', [
        ]);
    }

    public function history(){
        return $this->render('public/convert/history.html.twig', [
        ]);
    }
}
