<?php

namespace App\Controller\Api;

use App\Entity\Convert;
use App\Entity\Currency;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CurrencyController extends ApiController
{

    /**
     * @Route("/api/currency", name="api_currency")
     */
    public function index()
    {

        if(isset($_GET["currency_from"])) $currency_from = $_GET["currency_from"];
        else $currency_from = 'USD';

        if(isset($_GET["amount"])) $amount = $_GET["amount"];
        else $amount = 1;


        $currencyRepository = $this->getDoctrine()->getRepository(Currency::class)->findAll();

        $currencys = [];
        $entityManager = $this->getDoctrine()->getManager();

        foreach($currencyRepository as $currency){
            
            $url = $_ENV['API_KEY'].'&q='.$currency_from.'_'.$currency->getCurrency(); 

            $api = $this->callAPI('GET',$url,null);

            $data = json_decode($api, true);


            $imp = $data[$currency_from.'_'.$currency->getCurrency()];

            $currencys[] = [
                'id'        =>      $currency->getId(),
                'currency'  =>      $currency->getCurrency(),
                'imp'       =>      '1'.$currency->getCurrency().'= $'.number_format($imp, 2, '.', ','),
                'conv'      =>      '$'.number_format($amount * $imp, 2, '.', ','), 
            ];

            //create history

            if($amount > 1){
                $convert = new Convert();
                $convert->setAmount($amount);
                $convert->setCreatedAt(new DateTime('now'));
                $convert->setFromCurrency($currency_from);
                $convert->setToCurrency($currency);

                $entityManager->persist($convert);
                $entityManager->flush();
            }

            
        }




        return  $this->json($currencys);
    }

     /**
     * @Route("/api/history", name="api_history")
     */

    public function history()
     {
         $historyRepository = $this->getDoctrine()->getRepository(Convert::class)->findAll();

         $histories = [];

         foreach($historyRepository as $history){
            $histories[] = [
                'amount'    => $history->getAmount(),
                'from_currency'     => $history->getFromCurrency(),
                'to_currency'       => $history->getToCurrency()->getCurrency(),
                'created_at'        => date_format($history->getCreatedAt(), 'd-m-Y')
            ];
         }

        return  $this->json($histories);


     }
}
