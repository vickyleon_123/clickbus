/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.css';

// start the Stimulus application
import './bootstrap';

import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import BootstrapVue from 'bootstrap-vue'


import ConvertComponent from './controllers/Convert/ConvertComponent.vue';
import HistoryComponent from './controllers/Convert/HistoryComponent.vue';


Vue.component('convert-component', ConvertComponent);
Vue.component('history-component', HistoryComponent);

Vue.use(BootstrapVue)
Vue.use(VueAxios, axios)

axios.defaults.headers.common['Content-Type'] = 'application/json';


document.addEventListener('DOMContentLoaded', () => {

    if (document.getElementById("app")) {
        const app = new Vue({
            el: '#app'
        });
    }


});