<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210322222144 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE `convert` (id INT AUTO_INCREMENT NOT NULL, to_currency_id INT DEFAULT NULL, amount VARCHAR(255) NOT NULL, from_currency VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, INDEX IDX_B2BBA57F16B7BF15 (to_currency_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE currency (id INT AUTO_INCREMENT NOT NULL, currency VARCHAR(255) NOT NULL, image VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE `convert` ADD CONSTRAINT FK_B2BBA57F16B7BF15 FOREIGN KEY (to_currency_id) REFERENCES currency (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE `convert` DROP FOREIGN KEY FK_B2BBA57F16B7BF15');
        $this->addSql('DROP TABLE `convert`');
        $this->addSql('DROP TABLE currency');
    }
}
